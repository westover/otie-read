# Setup initial virtualenv to install packages
build_virtual_env:
	virtualenv env
# Oracle client has bugs with x64 on OSX
manipulate_python_for_oracle:
	mv env/bin/python env/bin/python.fat && lipo env/bin/python.fat -remove x86_64 -output env/bin/python
# Tar and move the required clients
oracle_config_linux:
	tar xvf oracle_linux.tar.gz . && mv oracle_linux oracle
# Tar and move the required clients
oracle_config_osx:
	tar xvf oracle_osx.tar.gz . && mv oracle_osx oracle
# Combine steps above and install requirements
setup_osx: build_virtual_env oracle_config_osx manipulate_python_for_oracle
	source setup_environment.sh && ./env/bin/pip install -r requirements.txt
# Combine steps above and install requirements
setup_linux: build_virtual_env oracle_config_linux
	source setup_environment.sh && ./env/bin/pip install -r requirements.txt

