OTIE READ Docs v2

This document is intended to be a guide to deployment and usage of the files contained in this repo.

Prerequisite python packages:

cx_Oracle (this has its own dependencies mainly the Oracle Instant Client and some system variables set)
Flask
Flask-Restful
Flask-CORS (not required under certain configurations)

#Quickstart:
**PREREQS** Python, python-dev, pip, virtualenv, and GNU make installed. included are configs for uwsgi and nginx in the wsgi directory

- Install Oracle Instant Client (complete with symlinks from `*.so.12.[0-9]*` to `*.so` or dylib for OS X (included are bundles for linux and mac)
- Configure setup_environment.sh with correct values (Current setup uses OTIE DB and a folder called oracle to house the instant client)
* `make setup`
* `source env/bin/activate`
* `python webapp.py`

#Installation procedure:

- Install Oracle Instant Client base and SDK packages and setting the ORACLE_HOME environment variable
- Install the python packages above

#Deployment strategies:

- Standalone listener:
    This package is capable with a stand alone listener by using python to execute the webapp.py file. 
    This is good for dev environments and for testing but isn't sustainable as realistically a dynamic web application should be run with a supervisor.
- uWSGI:
    This app can be run with uWSGI as a supervisor as well as implementing parallel techniques common in web server/web applications about handling concurrent requests.
    In addition this configuration can be put behind Nginx or other web server with a proxy pass.
- mod_WSGI:
    A more traditional web server can run this file directly with mod_WSGI this applies to both MS IIS and Apache2 webserver

No matter the deployment strategy the API is interfaced the same.

#API Interface:

The API has 3 basic endpoints:

Record:
    Usage:
        /record?id=$IDNUMBER
    Description:
        Pulls down a single record based on ID number specified.

Browse:
    Usage:
        /browse?dec=(Optional decision sector)&num_records=(Optional: number of records to return in this browse)&offset=(Optional: offset for pagination)&autocomplete=(True/False)
    Description:
        This endpoint is meant to return bulk records with minimal criteria. This can also be used to generate paginated results with the num_records and offset.
        Finally this endpoint is also capable of generating the suggestion list presented by the front end.

Search:
    Usage:
        /search?dec=(Optional decision sector)&query=(Optional: query string)&cost=(Optional: specific cost param)&os=(specific os param)
    Description:
        This is the main search endpoint which allows for sending in a query string and some specific parameters to allow for both the detail and basic search.
        
#Frontend Notes:

The front end is built on top of the EPA web template. It does incorporate some additional javascript jQuery-ui libraries which are publicly available but I have included them here as frozen versions.
All the custom javascript is contained in the read.js file which has the functions for communication with the API described above. The front end has been shown to work on many browsers but some older versions of IE have trouble.
The copy featured on the main page is approved by multiple members here at OTIE but is open to revision. The directory structure and things like that are also open to revision.