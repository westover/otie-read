__filename__ = ''
__author__ = 'westover'


import json

from flask import Flask
from flask.ext import restful
from flask.ext.restful import reqparse
from flask.ext.cors import CORS
import traceback
import re
from copy import deepcopy
from collections import OrderedDict
import cx_Oracle

app = Flask(__name__)
CORS(app, resources=r'/rest/read/*', headers='Content-Type')
api = restful.Api(app)

LOCAL = False

auto_complete_list = []

def oracle_query(query):
    print query
    connection = cx_Oracle.connect('read_admin', 'OTIEread',
                               '(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=read-oracle.ctgmhh3y9mtj.us-east-1.rds.amazonaws.com)(PORT=1521)))(CONNECT_DATA=(SID=ORCL)))')
    cursor = connection.cursor()
    cursor.execute(query)
    print 'Query Executed'
    columns = [
        'id', 'Date', 'User', 'Information_Resource_Identifier', 'Information_Resource_Title', 'Information_Resource_Short_Title',
        'Name', 'Acronym', 'Short_Description_for_Reports', 'Ownership_Type', 'Information_Resource_Type', 'Base_Cost_of_Software',
        'Other_Cost_Considerations', 'Open_Source', 'Alternative_Names', 'Sustainability_Sector',
        'Constrained_Keywords', 'Keywords', 'Organization', 'Contact_Person', 'Phone_Number', 'Email', 'Internet',
        'Life_Cycle', 'Last_Known_Software_Update', 'READ_Info_Updated', 'Operating_Environment', 'Compatible_Operating_Systems',
        'Other_proprietary_software_requirements_if_any', 'Model_Inputs', 'Data_requirements', 'Model_Output_Types',
        'Output_Variables', 'Source_of_Support_Materials', 'Types_of_Support_Materials', 'Model_Evaluation',
        'Model_Structure', 'Time_Scale', 'Spatial_Extent', 'Technical_skills_needed_to_apply_model',
        'Interfaces_to_other_Resources', 'Notes'
    ]
    output = [OrderedDict(zip(columns, this_row)) for this_row in cursor]
    for this_row in output:
        if this_row['Base_Cost_of_Software'] == 1:
            this_row['Base_Cost_of_Software'] = 'Free'
        elif this_row['Base_Cost_of_Software'] == 2:
            this_row['Base_Cost_of_Software'] = '$1-$499'
        elif this_row['Base_Cost_of_Software'] == 3:
            this_row['Base_Cost_of_Software'] = '$500-$1499'
        elif this_row['Base_Cost_of_Software'] == 4:
            this_row['Base_Cost_of_Software'] = '$1500-$3999'
        elif this_row['Base_Cost_of_Software'] == 5:
            this_row['Base_Cost_of_Software'] = '>$4000'
        for this_key in this_row:
            try:
                this_row[this_key] = this_row[this_key].replace(';',', ')
            except:
                pass
    return output

def mysql_query(query):
    try:
        import mysql.connector
    except:
        traceback.print_exc()
        return 'DATABASE ERROR'
    query += ';'
    if not LOCAL:
        connection = mysql.connector.connect(user = 'readdb', password = 'readdatabase', host = 'read.ctgmhh3y9mtj.us-east-1.rds.amazonaws.com', database = 'SHCRP')
    else:
        connection = mysql.connector.connect(user= 'root', password =  'root', host = 'localhost', database = 'SHCRP')
    cursor = connection.cursor()
    cursor.execute(query)
    columns = cursor.column_names
    # columns = [
    #     'Date', 'User', 'Information_Resource_Identifier', 'Information_Resource_Title', 'Information_Resource_Short_Title',
    #     'Name', 'Acronym', 'Short_Description_for_Reports', 'Ownership_Type', 'Information_Resource_Type', 'Base_Cost_of_Software',
    #     'Other_Cost_Considerations', 'Open_Source', 'Alternative_Names', 'Sustainability_Sector',
    #     'Constrained_Keywords', 'Keywords', 'Organization', 'Contact_Person', 'Phone_Number', 'Email', 'Internet',
    #     'Life_Cycle', 'Last_Known_Software_Update', 'READ_Info_Updated', 'Operating_Environment', 'Compatible_Operating_Systems',
    #     'Other_proprietary_software_requirements_if_any', 'Model_Inputs', 'Data_requirements', 'Model_Output_Types',
    #     'Output_Variables', 'Source_of_Support_Materials', 'Types_of_Support_Materials', 'Model_Evaluation',
    #     'Model_Structure', 'Time_Scale', 'Spatial_Extent', 'Technical_skills_needed_to_apply_model',
    #     'Interfaces_to_other_Resources', 'Notes'
    # ]
    output = [OrderedDict(zip(columns, this_row)) for this_row in cursor]
    for this_row in output:
        if this_row['Base_Cost_of_Software'] == 1:
            this_row['Base_Cost_of_Software'] = 'Free'
        elif this_row['Base_Cost_of_Software'] == 2:
            this_row['Base_Cost_of_Software'] = '$1-$499'
        elif this_row['Base_Cost_of_Software'] == 3:
            this_row['Base_Cost_of_Software'] = '$500-$1499'
        elif this_row['Base_Cost_of_Software'] == 4:
            this_row['Base_Cost_of_Software'] = '$1500-$3999'
        elif this_row['Base_Cost_of_Software'] == 5:
            this_row['Base_Cost_of_Software'] = '>$4000'
        for this_key in this_row:
            try:
                this_row[this_key] = this_row[this_key].replace(';',', ')
            except:
                pass
    return output


class Search(restful.Resource):
    def get(self):
        try:
            parser = reqparse.RequestParser()
            parser.add_argument('query', help = 'query string', type = str)
            parser.add_argument('dec', help = 'Decision sector', type = str)
            parser.add_argument('os', help = 'Op Environ', type = str)
            parser.add_argument('cost', help = 'Cost', type = str)
            parser.add_argument('extent', help = 'Extent of Model', type = str)
            parser.add_argument('outputs', help = 'Model Outputs', type = str)
            parser.add_argument('soft', help = 'system software', type = str)
            parser.add_argument('id', help = 'Lookup based on id', type = int)
            d = parser.parse_args()
            formatted_array = {this_key: d[this_key] for this_key in d if d[this_key] and d[this_key] != 'undefined'}
            json_keys = ['cost', 'extent', 'outputs', 'os']
            for this_key in json_keys:
                try:
                    formatted_array[this_key] = tuple(json.loads(formatted_array[this_key]))
                    if not formatted_array[this_key]:
                        del formatted_array[this_key]
                except:
                    pass
            if 'cost' in formatted_array and formatted_array['cost']:
                new_cost = []
                for this_cost in formatted_array['cost']:
                    if this_cost.lower() == 'free':
                        new_cost.append(1)
                    elif this_cost.lower() == '1':
                        new_cost.append(2)
                    elif this_cost.lower() == '500':
                        new_cost.append(3)
                    elif this_cost.lower() == '1500':
                        new_cost.append(4)
                    elif this_cost.lower() == '4000':
                        new_cost.append(5)
                formatted_array['cost'] = new_cost
            print formatted_array
            try:
                print self.build_query(formatted_array)
            except:
                traceback.print_exc()
            try:
                output_list = oracle_query(self.build_query(formatted_array))
            except:
                traceback.print_exc()
            return output_list
        except:
            pass
    def build_query(self, formatted_array):
        """
        :param formatted_array: This is the required entry of the parsed uri query string
        :return query string to be executed:
        """

        base_query = "SELECT * FROM tools WHERE"
        if 'dec' in formatted_array:
            sector_string = " lower(Sustainability_Sector) like '%{0}%' AND".format(formatted_array['dec'].lower())
            base_query += sector_string
        query_where_clause = "lower({0}) LIKE '%{1}%'"
        like_columns = [
            'Name', 'Acronym', 'Short_Description_for_Reports', 'Ownership_Type',
            'Other_Cost_Considerations', 'Open_Source', 'Alternative_Names', 'Sustainability_Sector',
            'Constrained_Keywords', 'Keywords', 'Organization', 'Contact_Person', 'Email', 'Internet',
            'Life_Cycle', 'Operating_Environment', 'Compatible_Operating_Systems',
            'Other_prop_sw_reqs', 'Model_Inputs', 'Data_requirements',
            'Model_Output_Types', 'Output_Variables', 'Source_of_Support_Materials', 'Types_of_Support_Materials',
            'Model_Evaluation', 'Model_Structure', 'Time_Scale', 'Spatial_Extent', 'Tech_skills',
            'Interfaces_to_other_Resources', 'Notes'
        ]
        and_columns = {
        'cost':'Base_Cost_of_Software', 'os':'Operating_Environment', 'outputs': 'Model_Output_Types',
        'extent':'Spatial_Extent', 'soft': ''
        }
        and_clauses = []
        for this_column in and_columns:
            if this_column in formatted_array and formatted_array[this_column]:
                if this_column == 'cost':
                    if len(formatted_array[this_column]) > 1:
                        this_string = and_columns[this_column] + ' IN ' + str(tuple(formatted_array[this_column]))
                    else:
                        this_string = and_columns[this_column] + '=' + str(formatted_array[this_column][0])
                    and_clauses.append(this_string)
                    continue
                if isinstance(formatted_array[this_column], tuple):
                    this_string = '\' OR \''.join(['%' + this_value + '%' for this_value in formatted_array[this_column]])
                else:
                    this_string = formatted_array[this_column]
                and_clauses.append(query_where_clause.format(and_columns[this_column], this_string.lower()).replace('%%', '%'))
        if and_clauses:
            base_query += ' (' + ' AND '.join(and_clauses) + ')'

        like_clauses = []
        if 'query' in formatted_array:
            if and_clauses:
                base_query += ' AND '
            for this_column in like_columns:
                like_clauses.append(query_where_clause.format(this_column, formatted_array['query'].lower()))
            base_query += ' (' + ' OR '.join(like_clauses) + ')'
        return base_query

class Record(restful.Resource):
    def get(self):
        try:
            parser = reqparse.RequestParser()
            parser.add_argument('id', help = 'query string', type = int)
            d = parser.parse_args()
            formatted_array = {this_key: d[this_key] for this_key in d if d[this_key] and d[this_key] != 'undefined'}
            try:
                print self.build_query(formatted_array)
            except:
                traceback.print_exc()
            try:
                output_list = oracle_query(self.build_query(formatted_array))
            except:
                traceback.print_exc()

            return output_list
        except:
            pass
    def build_query(self, formatted_array):
        """
        :param formatted_array: This is the required entry of the parsed uri query string
        :return query string to be executed:
        """

        base_query = "SELECT * FROM tools WHERE \"id\" = {0}".format(formatted_array['id'])
        return base_query

class Browse(restful.Resource):
    def get(self):
        global auto_complete_list
        autocomplete = False
        try:
            parser = reqparse.RequestParser()
            parser.add_argument('dec', help = 'Decision sector', type = str)
            parser.add_argument('num_records', help = 'query string', type = int)
            parser.add_argument('offset', help = 'query string', type = int)
            parser.add_argument('autocomplete', help = 'dummy variable to generate autocomplete')
            d = parser.parse_args()
            formatted_array = {this_key: d[this_key] for this_key in d if d[this_key] and d[this_key] != 'undefined'}
            if 'autocomplete' in formatted_array:
                if auto_complete_list:
                    print 'I have this cached'
                    return auto_complete_list
                else:
                    autocomplete = True
            try:
                print self.build_query(formatted_array)
            except:
                traceback.print_exc()
            try:
                output_list = oracle_query(self.build_query(formatted_array))
            except:
                traceback.print_exc()
            if autocomplete:
                print "Building word list"
                output_list = self.generate_autocomplete(output_list)
                auto_complete_list = deepcopy(output_list)
                print output_list[0:10]

            print len(output_list)
            return output_list
        except:
            pass

    def build_query(self, formatted_array):
        """
        :param formatted_array: This is the required entry of the parsed uri query string
        :return query string to be executed:
        """

        base_query = "SELECT * FROM tools"
        if 'dec' in formatted_array:
            sector_string = " WHERE lower(Sustainability_Sector) like '%{0}%'".format(formatted_array['dec'].lower())
            base_query += sector_string
        if 'num_records' in formatted_array:
            limit_string = " LIMIT {0}".format(formatted_array['num_records'])
            base_query += limit_string
            if 'offset' in formatted_array:
                offset_string = ", {0}".format(formatted_array['offset'])
                base_query += offset_string

        return base_query

    def generate_autocomplete(self, output_list):
        """
        :param output_list: output list from the browse query
        :return output_list which will also be cached to the auto_complete_list:
        """
        like_columns = [
            'Name', 'Acronym', 'Short_Description_for_Reports', 'Ownership_Type', 'Information_Resource_Type',
            'Other_Cost_Considerations', 'Open_Source', 'Alternative_Names', 'Sustainability_Sector',
            'Constrained_Keywords', 'Keywords', 'Organization', 'Contact_Person', 'Email', 'Internet',
            'Life_Cycle', 'Operating_Environment', 'Compatible_Operating_Systems',
            'Other_proprietary_software_requirements_if_any', 'Model_Inputs', 'Data_requirements',
            'Model_Output_Types', 'Output_Variables', 'Source_of_Support_Materials', 'Types_of_Support_Materials',
            'Model_Evaluation', 'Model_Structure', 'Time_Scale', 'Spatial_Extent', 'Technical_skills_needed_to_apply_model',
            'Interfaces_to_other_Resources', 'Notes'
        ]
        local_auto_complete_list = []
        collected_values = []
        print 'Collecting Values'
        for this_row in output_list:
            this_row = {this_key: this_row[this_key] for this_key in like_columns}
            local_auto_complete_list.append(this_row['Name'])
            local_auto_complete_list.append(this_row['Acronym'])
            collected_values += filter(lambda this_value: str(this_value) and this_value, this_row.values())
        collected_values = list(set(collected_values))
        print 'Forming and refining blob collected_values:', len(collected_values)
        refined_blob = ''
        for blob in collected_values:
            refined_blob += ' '
            try:
                blob = str(blob)
            except:
                pass
            if blob.lower() in ('http', 'https', 'the'):
                continue
            for this_char in blob:
                if this_char.isalpha() or this_char.isspace():
                    refined_blob += this_char
                else:
                    refined_blob += ' '
        print 'Further Refining blob', len(refined_blob)
        temp_list = re.split('\s+', refined_blob)
        print 'Building the final list', len(temp_list)
        return list(set([element.lower() for element in list(set(temp_list)) if len(element) > 1] + local_auto_complete_list))

api.add_resource(Search, '/rest/read/search')
api.add_resource(Record, '/rest/read/record')
api.add_resource(Browse, '/rest/read/browse')


application = app
if __name__ == '__main__':
   app.run(host = '0.0.0.0', port = 8080, debug = True)
