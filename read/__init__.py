from flask import Flask, render_template
from flask.ext import restful
from flask.ext.cors import CORS
from controller import *

app = Flask(__name__, static_url_path='')
CORS(app, resources=r'/rest/read/*', headers='Content-Type')
# Placeholder for when we replace this with models rather than query builder
# db.init_app(app)

api = restful.Api(app)

api.add_resource(Search, '/rest/read/search')
api.add_resource(Record, '/rest/read/record')
api.add_resource(Browse, '/rest/read/browse')

@app.route('/')
def index():
    return app.send_static_file('index.html')

